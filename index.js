// 1. What directive is used by Node.js in loading the modules it needs?

      // Answer: "require" directive

// 2. What Node.js module contains a method for server creation?

      // Answer: http module
  

// 3. What is the method of the http object responsible for creating a server using Node.js?

      // Answer: createServer() method
  

// 4. What method of the response object allows us to set status codes and content types?
	
       // Answer: writeHead()


// 5. Where will console.log() output its contents when run in Node.js?

       // Answer: When server is running, console will print the message in the terminal/bash
       

// 6. What property of the request object contains the address's endpoint?
       // Answer: parameters


// Import the http module using the required directive.
let http = require("http");

//Create a variable port and assign it with the value of 3000.
let port = 3000;

//Create a server using the createServer method that will listen in to the port provided above.
http.createServer(function(request, response) {
      response.writeHead(200, {"Content-Type": "text/plain"});
      response.end("Hello World");
}).listen(port);
console.log("Server running at localhost:3000");